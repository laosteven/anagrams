# Optimized Anagram Counter #
-------

Count how much **[anagrams](https://en.wikipedia.org/wiki/Anagram)** that can be found on each line of a text file based on a custom dictionary file.

The algorithm will need two information:

1. `words.txt`: the text file which the algorithm will count how many possible anagrams there is on each line.
2. `dict.txt`: another text file that contains all the known words that exists from the english dictionary.  

For example, here is a console output of the results:

```
#!
Il y a 4 anagrammes du mot marion
Il y a 8 anagrammes du mot argent
Il y a 5 anagrammes du mot crane
Il y a 3 anagrammes du mot aimer
Il y a 1 anagrammes du mot soigneur
Il y a 5 anagrammes du mot ordinateur
Il y a un total de 26 anagrammes
Total d'execution 0.921123981 millisecondes
```

At the end of the console output, the algorithm will also specify the **total amount** of anagrams found along with the time it took for performance reasons.

# Objectives #
-------
* Determine the **weight** of each line of the algorithm; the time it takes to complete and optimize it, if possible.
* Find out how much an optimal code can affect performance by stress-testing everything.

# How do I get set up? #
-------
* Download the runnable `.jar`, `words.txt` and `dict.txt` files in the project's root folder.
* Save it in an easily accessible folder location.
* Open the command prompt and write:
  `java -jar anagrams.jar words.txt dict.txt`

# For more information #
-------
Visit the following website: [**Data structure and Algorithms** (LOG320)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=log320) [*fr*]