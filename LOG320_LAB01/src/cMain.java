import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class cMain
{
	public static void main(String[] args) throws IOException
	{
		int nombreAnagram = 0;
		int totalAnagram = 0;

		FileReader wordFile = new FileReader(args[0]);
		FileReader dictioFile = new FileReader(args[1]);

		BufferedReader brWord = new BufferedReader(wordFile);
		String lineWord;

		BufferedReader brDict = new BufferedReader(dictioFile);
		String lineDict;

		List<String> dictList = new ArrayList<String>();
		while ((lineDict = brDict.readLine()) != null)
		{
			lineDict = lineDict.replaceAll("\\s", "");
			dictList.add(lineDict);
		}

		long startTime = System.nanoTime();
		while ((lineWord = brWord.readLine()) != null)
		{
			for (String dict : dictList)
			{
				if (EstUnAnagrammeOriginal(lineWord, dict))
				{
					nombreAnagram++;
				}
			}

			System.out.println("Il y a " + nombreAnagram + " anagrammes du mot " + lineWord);
			totalAnagram += nombreAnagram;

			nombreAnagram = 0;
		}
		long endTime = System.nanoTime();
		long duration = (endTime - startTime) / 1000;

		System.out.println("Il y a un total de " + totalAnagram + " anagrammes");
		System.out.println("Total d'execution " + duration + " microsecondes");

	}

	public static boolean EstUnAnagrammeOriginal(String pWord, String pDict)
	{
		boolean trouve = false;
		for (int i = 0; i < pWord.length(); i++)
		{
			trouve = false;
			for (int j = 0; j < pDict.length(); j++)
			{
				if (pWord.charAt(i) == pDict.charAt(j) && !trouve)
				{
					pDict = new StringBuilder(pDict).deleteCharAt(j).toString();
					j--;
					trouve = true;
				}
			}
			if (!trouve)
			{
				return trouve;
			}
		}
		if (pDict != null && !pDict.isEmpty())
		{
			return false;
		}
		return trouve;
	}

	public static boolean EstUnAnagrammeMeilleur(String s1, String s2)
	{
		return false;
	}
}
